import 'package:flutter/material.dart';
import 'package:social_pet/screens/pet_center/PetNavigation.dart';


void main() => runApp(StartApp());

class StartApp extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      theme: ThemeData(
        primaryColor: Color(0xFFFF5338),
        accentColor: Colors.amber,
        accentColorBrightness: Brightness.dark
      ),
      routes: {
        '/': (context) => PetNavigation(),
      }
    );
  }
}
