import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PetMap extends StatefulWidget {
  @override 
  _PetMapState createState() => _PetMapState();

}

class _PetMapState extends State<PetMap> {
  Completer<GoogleMapController> _controller = Completer();

  static const LatLng _center = const LatLng(45.9624471, 5.5109123);

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Map'),
        ),
        body: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 11.0,
          ),
        )
        );
  }
}
